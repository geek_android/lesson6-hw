package ru.gooddocs.vladislav.cardview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SimpleListActivity extends AppCompatActivity {

    private ArrayAdapter<String> adapter;
    @BindView(R.id.list_view1) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);
        ButterKnife.bind(this);

        initUi();
    }

    private void initUi() {
        adapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1);

        addDataToAdapter(adapter);
        listView.setAdapter(adapter);
    }

    @OnClick(R.id.fab)
    public void buttonClick() {
        addDataToAdapter(adapter);
        listView.smoothScrollToPosition(adapter.getCount() - 10);
    }

    private void addDataToAdapter(ArrayAdapter<String> adapter) {
        for (int i = 0; i < 1000; i++) {
            adapter.add("item " + i);
        }
        adapter.notifyDataSetChanged();
    }
}
