package ru.gooddocs.vladislav.cardview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mRecyclerView) RecyclerView rv;
    CustomRVAdapter rvAdapter;
    private List<CustomModelCard> mCustomModelCardsList;
    private int listSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        listSize = 5;
        initUi();
    }

    private void initUi() {
        initRecycler();
    }

    private void initRecycler() {
        if (rv != null) {
//            GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rv.setLayoutManager(layoutManager);


            mCustomModelCardsList = new ArrayList<>();
            for (int i = 0; i <= listSize; i++) {
                mCustomModelCardsList.add(new CustomModelCard("Title " + i, "Description " + i, getImgID(i)));
            }

            rvAdapter = new CustomRVAdapter(mCustomModelCardsList);
            rv.setAdapter(rvAdapter);
            rvAdapter.notifyDataSetChanged();
        }
    }

    private int getImgID(int i) {
        int imgID;
        if (i % 2 == 0) {
            imgID = R.drawable.nochnoj_gorod;
        } else {
            imgID = R.drawable.zemlya_luna;
        }
        return imgID;
    }

    @OnClick(R.id.fab2)
    public void clickFAB() {
        ++listSize;
        mCustomModelCardsList.add(new CustomModelCard("Title " + listSize, "Description " + listSize, getImgID(listSize)));
        rvAdapter.notifyDataSetChanged();
    }


    @OnLongClick(R.id.fab2)
    public boolean longClickFAB() {
        List<CustomModelCard> deleteCustomModelCards = new ArrayList<>();
        List<Integer> selectedtems = rvAdapter.getSelectedItems();

        for (Integer selectedtem : selectedtems) {
            deleteCustomModelCards.add(mCustomModelCardsList.get(selectedtem));
        }
        rvAdapter.cleanSelectedItems();
        boolean result = mCustomModelCardsList.removeAll(deleteCustomModelCards);
        rvAdapter.notifyDataSetChanged();
        return result;
    }
}
