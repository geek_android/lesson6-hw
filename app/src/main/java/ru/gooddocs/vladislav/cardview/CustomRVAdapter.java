package ru.gooddocs.vladislav.cardview;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class CustomRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<Integer> selectedItems;
    private List<CustomModelCard> mListCard;

    CustomRVAdapter(List<CustomModelCard> mListCard) {
        this.mListCard = mListCard;
        selectedItems = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == 3) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card2, parent, false);
            return new CardViewHolder2(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
            return new CardViewHolder1(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CustomModelCard currentCustomModelCard = mListCard.get(position);

        switch (holder.getItemViewType()) {
            case 0:
                CardViewHolder1 cardViewHolder1 = (CardViewHolder1) holder;
                cardViewHolder1.tvTitle.setText(currentCustomModelCard.getTitle());
                cardViewHolder1.tvDescription.setText(currentCustomModelCard.getDescription());
                cardViewHolder1.imgView.setImageResource(currentCustomModelCard.getImgId());
                applySelection(cardViewHolder1, position);
                setListenerToHolder(cardViewHolder1, position);
                break;
            case 3:
                CardViewHolder2 cardViewHolder2 = (CardViewHolder2) holder;
                cardViewHolder2.tvTitle2.setText(currentCustomModelCard.getTitle());
                cardViewHolder2.tvDescription2.setText(currentCustomModelCard.getDescription());
                cardViewHolder2.imgView2.setImageResource(currentCustomModelCard.getImgId());
                applySelection(cardViewHolder2, position);
                setListenerToHolder(cardViewHolder2, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position % 3 == 0) return 3;
        else return 0;
    }

    @Override
    public int getItemCount() {
        return mListCard.size();
    }

    private void setListenerToHolder(CardViewHolderChangable c, int position) {
        c.setListener(v -> {
            Integer id = position;
            if (selectedItems.contains(id)) {
                selectedItems.remove(id);
            } else {
                selectedItems.add(id);
            }
            applySelection(c, id);
        });
    }

    public ArrayList<Integer> getSelectedItems() {
        return selectedItems;
    }

    public boolean cleanSelectedItems() {
        selectedItems.clear();
        return selectedItems.isEmpty();
    }

    private void applySelection(CardViewHolderChangable cardViewHolderChangable, int position) {
        if (selectedItems.contains(position)) {
            cardViewHolderChangable.changeTextColor(Color.RED);
        } else {
            cardViewHolderChangable.changeTextColor(Color.BLACK);
        }
    }


    interface CardViewHolderChangable {
        void changeTextColor(int color);

        void setListener(View.OnClickListener listener);
    }


    class CardViewHolder1 extends RecyclerView.ViewHolder implements CardViewHolderChangable {
        ImageView imgView;
        TextView tvTitle;
        TextView tvDescription;

        CardViewHolder1(View view) {
            super(view);
            imgView = view.findViewById(R.id.thumbnail);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvDescription = view.findViewById(R.id.tvDescr);
        }

        @Override
        public void changeTextColor(int color) {
            tvTitle.setTextColor(color);
            tvDescription.setTextColor(color);
        }

        @Override
        public void setListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }


    }

    class CardViewHolder2 extends RecyclerView.ViewHolder implements CardViewHolderChangable {

        ImageView imgView2;
        TextView tvTitle2;
        TextView tvDescription2;

        CardViewHolder2(View view) {
            super(view);
            imgView2 = view.findViewById(R.id.thumbnail2);
            tvTitle2 = view.findViewById(R.id.tvTitle2);
            tvDescription2 = view.findViewById(R.id.tvDescr2);
        }

        @Override
        public void setListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }

        @Override
        public void changeTextColor(int color) {
            tvTitle2.setTextColor(color);
            tvDescription2.setTextColor(color);
        }
    }

}
