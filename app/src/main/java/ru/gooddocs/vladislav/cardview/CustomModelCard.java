package ru.gooddocs.vladislav.cardview;

/**
 * Created by vlad on 07.02.18.
 */

public class CustomModelCard {

    private String mTitle;
    private String mDescription;
    private int mImageID;

    public CustomModelCard(String title, String description, int img) {
        mTitle = title;
        mDescription = description;
        mImageID = img;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getImgId() {
        return mImageID;
    }
}
